import "./App.css";
import BookListContainer from './components/BookListContainer';
import Login from './pages/Login';
import { LoginProvider } from "./UserContext";
import Home from './pages/Home';
import { Route, Routes } from "react-router-dom";
import Register from './pages/Register';
import Nav from './components/Nav';

const App = () => {

    return (
    <div style={{marginTop: '-45px'}}>
        <LoginProvider>
            <Nav />
                <Routes>
                    <Route path="/" exact element={<Home />} />
                    <Route path="/login" element={<Login />} />
                    <Route path="/register" element={<Register />} />
                    <Route path="/booklistcontainer" element={<BookListContainer />} />
                </Routes>
        </LoginProvider>
</div>
    )
}

export default App;
