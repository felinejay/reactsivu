import {Link} from "react-router-dom";
import { useContext } from "react";
import { LoginContext } from "../UserContext";

const Nav = () => {
    const {token} = useContext(LoginContext);


     return (
        <nav className="navbar navbar-expand-md navbar-dark bg-dark mb-4">
        <div className="container-fluid">
            <a className="navbar-brand" href="/">Home</a>
            <ul className="navbar-nav me-auto mb-2 mb-md-0">
                <li className="nav-item">
                    {token ? "" : <Link to="/login" className="nav-link active">Login</Link>}
                </li>
                <li className="nav-item">
                    {token ? "" : <Link to="/register" className="nav-link active">Register</Link>}
                </li>
            </ul>
            </div>
        </nav>
    );
};

export default Nav;