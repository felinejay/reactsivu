import { List, Select, MenuItem, InputLabel } from "@mui/material";
import { useEffect, useState } from "react";
import SaveBook from "./SaveBook";
import BookList from "./BookList";


const BookListContainer = () => {

    const [rBookList, setRBookList] = useState([]);
    const [category, setCategory] = useState("All");

    
    useEffect(() => {
        const getBook = async (e) => {
            const result = await fetch('http://localhost:3004/books', {
                method: 'GET',
                headers: {'Content-Type': 'application/json'}
            });
            if(result.status === 200 || result.status === 304) {
                const content = await result.json();
                setRBookList(content);
            } else {
                alert("Invalid credentials")
            }
        }
        getBook();
    }, [])


     const handleChange = (event) => {
       setCategory(event.target.value);
    };


    let saveBook;
    if (localStorage.getItem("userID") === "1" || localStorage.getItem("userID") === "4" || localStorage.getItem("userID") === "7") {
      saveBook = <SaveBook />;
    } else {
      saveBook = <h3>You need to log in as an Administrator to access the book saving feature.</h3>;
    }

return (
    <>
    {saveBook}
    <List key={Math.random() * 50} className="bookList">
        <h1>List of books to read</h1>
        <p>Click the books to open info</p>
        <InputLabel id="select-label">Category</InputLabel>
        <Select labelId="select-label" value={category} onChange={handleChange} label="Category">
            <MenuItem value="All">All</MenuItem>
            <MenuItem value="Scifi" id="scifi">Scifi</MenuItem>
            <MenuItem value="Fantasy" id="fantasy">Fantasy</MenuItem>
            <MenuItem value="Horror" id="horror">Horror</MenuItem>
        </Select>
        <BookList rawData={rBookList} category={category} />
</List>
 
    
    </>
)};

export default BookListContainer;
