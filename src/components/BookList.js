import { ListItem } from '@mui/material';
import { useState, useEffect } from 'react';
import Book from './Book';


const mapBooks = (newdata) => { 
    return (
        <ListItem key={newdata.id}>
            <Book data={newdata}/>
        </ListItem>
    );
}

const BookList = (props) => {
    const [bookList, setBookList] = useState(props.rawData.map(mapBooks));

    
     useEffect(() => {
        if(props.category === "Fantasy") {
            setBookList(props.rawData.filter((book) => {
                return book.category === "Fantasy";
                }).map(mapBooks))
        } else if(props.category === "Scifi") {
            setBookList(props.rawData.filter((book) => {
                return book.category === "Scifi";
            }).map(mapBooks))
        } else if (props.category === "Horror") {
            setBookList(props.rawData.filter((book) => {
                return book.category === "Horror";
            }).map(mapBooks))
        } else {
            setBookList(props.rawData.map(mapBooks)) 
        }
     }, [props.rawData, props.category]);

    return (
        <>
            {bookList}
        </>
    )
}

export default BookList;