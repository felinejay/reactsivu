import { useState } from 'react';
import BookItem from "./BookItem";
import { Checkbox } from "@mui/material";

const Book = (data) => {
    const [isChecked, setIsChecked] = useState(false);

    const toggleCheckbox = () => {
        setIsChecked(!isChecked);
    }

    return (
        <>
            <Checkbox 
            id="checkers"
            checked={isChecked}
            onChange={toggleCheckbox}
            className="checkbox"/>
            <BookItem 
            description={data.data.description} 
            title={data.data.title} 
            author={data.data.author} 
            published={data.data.published}
            id={data.data.id}
            isTaken={data.data.isTaken}
            category={data.data.category}
             />
        </>
    )
}

export default Book;
