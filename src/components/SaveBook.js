import React, { useState } from 'react'
import { Button, Paper, Grid, TextField } from "@mui/material";

function SaveBook() {


    const [title, setTitle] = useState("");
    const [author, setAuthor] = useState("");
    const [isTaken, setIsTaken] = useState("");
    const [published, setPublished] = useState("");
    const [category, setCategory] = useState("");
    const [description, setDescription] = useState("");

    const submitBook = async (e) => {
        e.preventDefault();
        const result = await fetch('http://localhost:3004/books', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                title,
                author,
                isTaken,
                published,
                category,
                description
            })
        });
        if(result.status === 201) {
            const content = await result.json();
            console.log(content);

        } else {
            alert("Invalid book")
        }
    }

    const paperStyle = {padding: 20, height: "85vh", width: 320, margin: "20px auto"}

    return (
        <div className="formContainer">
        <Grid>
                <Paper elevation={10} style={paperStyle}>
                    <Grid align="center">
                    <div className="form-inner">
                        <h2>Add book</h2>
                    </div>
                
                </Grid>
                <form>
    
                    <TextField variant="standard" id="bookName" onChange={e => setTitle(e.target.value)} label="Book name" placeholder="Enter book name" fullWidth />
                    <TextField variant="standard" id="bookAuthor" onChange={e => setAuthor(e.target.value)}label="Book author" placeholder="Enter author" fullWidth />
                    <TextField variant="standard" id="bookPubl" onChange={e => setPublished(e.target.value)} label="Book's publish year" placeholder="Enter year" fullWidth />
                    <TextField variant="standard" id="bookCategory" onChange={e => setCategory(e.target.value)} label="Book category" placeholder="Enter category" fullWidth />
                    <TextField variant="standard" id="bookDesc" onChange={e => setDescription(e.target.value)} label="Book description" placeholder="Enter description" fullWidth />
                    <TextField variant="standard" id="bookAvailability" onChange={e => setIsTaken(e.target.value)} label="Book availability" placeholder="Available/Not available" fullWidth />
                    <div></div>
                    <Button variant="contained" color="success" onClick={submitBook} fullWidth>Save book</Button>
                    </form>
                </Paper>
            </Grid>
    
        </div>
    )
}

export default SaveBook
