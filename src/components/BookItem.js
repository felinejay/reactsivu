import { Card, Collapse } from "@mui/material";
import { useState } from "react";


const BookItem = (book) => {

    const [showDesc, setShowDesc] = useState(false);
    const showDescription = () => {
        setShowDesc(!showDesc);
    }

    return (

            <Card key={book.id} className="card" onClick={showDescription}>
                <h3>{book.title}</h3>
                <b>{showDesc ? book.isTaken : "Availability"}</b>
                <Collapse in={showDesc} unmountOnExit>
                <p>{showDesc ? book.author : null}</p>
                <p>{showDesc ? book.category : null}</p>
                <p><span className="notbold">{showDesc ? book.published : null}</span></p>
                <div id={book.id}><span className="notbold">{showDesc ? book.description : null}</span></div>
                </Collapse>
            </Card>

    )
};

export default BookItem;