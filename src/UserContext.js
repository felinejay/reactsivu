import { createContext, useState, useEffect } from "react";

export const LoginContext = createContext({ 
    userID: "",
    username: "",
    token: "",
    saveUser: () => null,
    handleToken: () => null
});

export const LoginProvider = (props) => {
    const {children} = props;
    const [userID, setUserID] = useState("");
    const [username, setUserName] = useState("");
    const [token, setToken] = useState("");

    useEffect(() => {
        const loggedUserID = localStorage.getItem("userID");
        const loggedUsername = localStorage.getItem("username");
        const loggedToken = localStorage.getItem("token");

        if (loggedUserID && loggedUsername && loggedToken) {
            setUserID(loggedUserID);
            setUserName(loggedUsername);
            setToken(loggedToken);
        }
    }, []);

    const saveUser = (userID, username) => {
        localStorage.setItem("userID", JSON.stringify(userID));
        localStorage.setItem("username", JSON.stringify(username));
        setUserID(userID);
        setUserName(username);
    }

    const handleToken = (token) => {
        localStorage.setItem("token", JSON.stringify(token));
        setToken(token);
    }

    return(
        <LoginContext.Provider value={{userID, username, token, saveUser, handleToken}}>
            {children}
        </LoginContext.Provider>
    )
}