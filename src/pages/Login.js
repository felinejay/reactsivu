import React, { useState, useContext } from 'react';
import { LoginContext } from "../UserContext";
import { Button, Grid, Paper, Avatar, TextField, FormControlLabel, Checkbox } from "@mui/material";
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import { Navigate, Link } from 'react-router-dom';
import "../App.css";


export default function Login() {
    const {handleToken, saveUser} = useContext(LoginContext);
    const paperStyle = {padding: 20, height: "70vh", width: 320, margin: "20px auto"}
    
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [redirect, setRedirect] = useState(false);

    const submit = async (e) => {
        e.preventDefault();
        const result = await fetch('http://localhost:3004/login', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                "email": email,
                "password": password
            })
        });
        if(result.status === 200) {
            const content = await result.json();
            handleToken(content.accessToken);
            saveUser(content.user.id, content.user.name);
            setRedirect(true);
        } else {
            alert("Invalid credentials")
        }
    }

    if(redirect) {
        return <Navigate to='/'/>
    }

    return (
        <Grid>
            <Paper elevation={10} style={paperStyle}>
                <Grid align="center">
            <Avatar style={{ backgroundColor: "darkred" }}><LockOutlinedIcon style={{ fontSize: "30px", justifyContent: "center" }}/></Avatar>
                <div className="form-inner">
                    <h2>Login</h2>
                </div>
            
            </Grid>
            <form onSubmit={submit}>

                <TextField variant="standard" label="Email" placeholder="Enter email" onChange={e => setEmail(e.target.value)} fullWidth required/>
                <TextField variant="standard" label="Password" type="password" placeholder="Enter password" onChange={e => setPassword(e.target.value)}fullWidth required/>
                <FormControlLabel control={<Checkbox />} label="Remember me" />
                <Button type="submit" variant="contained" fullWidth>Log in</Button>
                <Link to="/register">No account yet? Create one!</Link>
                </form>
            </Paper>
        </Grid>
    )
}
