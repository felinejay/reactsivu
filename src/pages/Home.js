import { useContext } from 'react';
import { LoginContext } from "../UserContext";
import BookListContainer from './../components/BookListContainer';
import { Button } from "@mui/material";

const Home = () => {
    const {token, username} = useContext(LoginContext);

    const logout = () => {
        localStorage.removeItem('token');
        localStorage.removeItem('username');
        localStorage.removeItem('userID');
        window.location.reload();
    }
    return (
        <div>
            <h2>{token ? "Hey " + username + "!" : "You need to log in."}</h2>
            {token ? <Button style={{ display: 'flex' }} variant="contained" onClick={logout}>LOGOUT</Button> : ""}
            {token ? <BookListContainer /> : ""}
        </div>
    )
}

export default Home;